﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Collections.Generic;

namespace RainingMen
{
    public class RainingMen : Script
    {
        int maxSpawnedPeds;
        int painType;
        bool pedsScream;

        List<Ped> spawnedPeds = new List<Ped>();
        Random rand = new Random();

        public RainingMen()
        {
            pedsScream = Settings.GetValue("Settings", "Peds_Scream", true);
            painType = Settings.GetValue("Settings", "Scream_Type", 8); //8 == on fire scream
            maxSpawnedPeds = Math.Max(Math.Min(Settings.GetValue("Settings", "Max_Spawned_Peds", 30), 50), 1);

            Interval = Math.Max(Math.Min(Settings.GetValue("Settings", "Interval", 750), 2000), 50);
            Tick += RainingMen_Tick;
        }

        private void processSpawnedPeds()
        {
            //delete first ped in array when at max spawned peds
            if (spawnedPeds.Count > maxSpawnedPeds)
            {
                if (spawnedPeds[0] != null && spawnedPeds[0].Exists())
                {
                    for (int i = 255; i > 5; i -= 10)
                    {
                        spawnedPeds[0].Alpha = i;
                        Yield();
                    }

                    spawnedPeds[0].Model.MarkAsNoLongerNeeded();
                    spawnedPeds[0].Delete();
                }

                spawnedPeds.RemoveAt(0);
            }

            //kill peds when they hit the ground or aren't ragdolled anymore (otherwise you'll have a frick-ton of dudes just standing there)
            foreach (Ped ped in spawnedPeds)
            {
                if ((ped.HeightAboveGround < 2f || !ped.IsRagdoll) && ped.IsAlive)
                {
                    ped.Kill();
                }
            }
        }

        private void RainingMen_Tick(object sender, EventArgs e)
        {
            Ped plrPed = Game.Player.Character;

            if (plrPed != null && plrPed.Exists() && plrPed.IsAlive)
            {
                var currentWeather = World.Weather;

                if (currentWeather == Weather.Raining || currentWeather == Weather.ThunderStorm)
                {
                    processSpawnedPeds();

                    //spawn raining men
                    var velOffset = Vector3.Zero;

                    if (plrPed.IsInVehicle())
                    {
                        var veh = plrPed.CurrentVehicle;

                        if (veh != null && veh.Exists() && veh.IsAlive)
                        {
                            velOffset = veh.Velocity * 3f;
                        }
                    }

                    var spawnPos = plrPed.GetOffsetInWorldCoords(new Vector3(0f, 0f, 50f)).Around(rand.Next(10, 30)) + velOffset;

                    Ped ped = World.CreatePed(MaleModels.models[rand.Next(0, MaleModels.models.Length)], spawnPos);

                    int tries = 0;
                    while ((ped == null || !ped.Exists()) && tries++ < 20) Wait(50);

                    if (ped != null && ped.Exists())
                    {
                        ped.Money = 0;
                        ped.Health = 1;

                        ped.CanRagdoll = true;
                        Function.Call(Hash.SET_PED_TO_RAGDOLL, ped, -1, -1, 0, false, false, false);

                        ped.ApplyForce(new Vector3(0f, 0f, -50f), new Vector3(rand.Next(-5, 5), rand.Next(-5, 5), rand.Next(-5, 5)));

                        if (pedsScream) Function.Call(Hash.PLAY_PAIN, ped, painType, 0, 0); 

                        ped.MarkAsNoLongerNeeded();

                        spawnedPeds.Add(ped);
                    }
                }
            }
            else
            {
                if (spawnedPeds.Count != 0)
                {
                    //delete all spawn peds
                    foreach (Ped ped in spawnedPeds)
                    {
                        ped.Model.MarkAsNoLongerNeeded();
                        ped.Delete();
                    }

                    spawnedPeds.Clear();
                }
            }
        }
    }
}
